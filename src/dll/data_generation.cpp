#include <iostream>
#include <windows.h>
#include <D3D11.h>
#include <DXGI.h>
#include <D3DX11tex.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

// Create our virtual GPU, entry-point for D3D11 API
ID3D11Device* g_pDevice = nullptr;

// Swap Chain holding our front and back buffer(s) - We capture the final stage of the back buffer
IDXGISwapChain* g_pSwapChain = nullptr;

// vGPU context - set rendering parameters/commands
ID3D11DeviceContext* g_pContext = nullptr;

// Where the GPU renders content
ID3D11RenderTargetView* g_pRenderTargetView = nullptr;

// Buffers/resouces to hold screen data
ID3D11Texture2D* pCaptureBuffer = nullptr;


void CaptureFrame() {
    if (!g_pContext || !g_pRenderTargetView) return;

    ID3D11Texture2D* processBackBuffer;
    HRESULT hr = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&processBackBuffer);

    if (SUCCEEDED(hr)) {
        D3D11_TEXTURE2D_DESC desc;
        processBackBuffer->GetDesc(&desc);

        // Create a new texture to hold the copied data
        desc.BindFlags = 0;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
        desc.Usage = D3D11_USAGE_STAGING;

        ID3D11Texture2D* processCopyTexture;
        g_pDevice->CreateTexture2D(&desc, NULL, &processCopyTexture);
        if (processCopyTexture) {
            g_pContext->CopyResource(processCopyTexture, processBackBuffer);

            // TODO: Find out what to do with the rendered frame

            processCopyTexture->Release();
        }

        processBackBuffer->Release();
    }
}


BOOL APIENTRY DllMain(HMODULE hmodule, DWORD ul_reason_for_call, LPVOID lpReserverd) {
    switch (ul_reason_for_call) {
        case DLL_PROCESS_ATTACH:
            break;
        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
            break;
        case DLL_PROCESS_DETACH:
            // TODO: Cleanup
            if (g_pRenderTargetView) g_pRenderTargetView->Release();
            if (g_pSwapChain) g_pSwapChain->Release();
            if (g_pContext) g_pContext->Release();
            if (g_pDevice) g_pDevice->Release();
            break;
    }
    
    return true;
}