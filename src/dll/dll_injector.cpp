#include <iostream>
#include <windows.h>
#include <tlhelp32.h>
#include <string>


int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: dll_injector.exe <pid> <path_to_dll>" << "\n";
        return 1;
    }

    DWORD pid = std::stoi(argv[1]);
    const char* dllPath = argv[2];
    
    // 1. Grab a handle (index) to current process
    HANDLE h_process = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
    if (!h_process) {
        std::cerr << "Failed to open process: " << pid << "\n";
        return 1;
    }

    // 2. To avoid messing with the games memory, we manually allocate memory inside the process
    void* p_dllPath= VirtualAllocEx(h_process, 0, strlen(dllPath) + 1, MEM_COMMIT, PAGE_READWRITE);
    if (!p_dllPath) {
        std::cerr << "Failed to allocate memory in the target process." << "\n";
        CloseHandle(h_process);
        return 1;
    }

    // 3. Write the path of the DLL into the target process's memory
    BOOL processDllMemory = WriteProcessMemory(h_process, p_dllPath, dllPath, strlen(dllPath) + 1, 0);
    if (!processDllMemory) {
        std::cerr << "Failed to write to target processs's memory." << "\n";
        VirtualFreeEx(h_process, p_dllPath, 0, MEM_RELEASE);
        CloseHandle(h_process);
        return 1;
    }

    // 4. Create a separate thread inside of the target process, loading our DLL into the process
    HANDLE h_thread = CreateRemoteThread(h_process, 0, 0, (LPTHREAD_START_ROUTINE)LoadLibraryA, p_dllPath, 0, 0);
    if (!h_thread) {
        std::cerr << "Failed to create a thread inside the target process." << "\n";
    } else {
        // 5. When the thread is finished, we are good to clean-up
        WaitForSingleObject(h_thread, INFINITE);

        // Check if the DLL was injected
        DWORD exitCode = 0;
        GetExitCodeThread(h_thread, &exitCode);
        if (exitCode == 0) {
            std::cerr << "DLL was not loaded by the target process." << "\n";
        }
    }

    // 6. Clean Up
    CloseHandle(h_thread);
    VirtualFreeEx(h_process, p_dllPath, 0, MEM_RELEASE);
    CloseHandle(h_process);

    return 0;
}